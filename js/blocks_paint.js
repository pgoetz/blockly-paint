Blockly.Blocks['draw_line'] = {
  init: function() {
    this.jsonInit({
      "message0": "draw line from %1 / %2 to %3 / %4",
      "args0": [
        {
          "type": "input_value",
          "name": "from_x",
          "check": "Number"
        },
        {
          "type": "input_value",
          "name": "from_y", 
          "check": "Number"
        },
        {
          "type": "input_value",
          "name": "to_x", 
          "check": "Number"
        },
        {
          "type": "input_value",
          "name": "to_y", 
          "check": "Number"
        }
      ],
      "inputsInline": true,
      "previousStatement": null,
      "nextStatement": null,
          "colour": 30,
      "tooltip": "Draws a line from the specified point to the target coordinate.",
      "helpUrl": "https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/lineTo"
    });
  }
};

Blockly.Blocks['draw_circle'] = {
  init: function() {
    this.jsonInit({
      "message0": "draw circle with radius %3 at position %1 / %2",
      "args0": [
        {
          "type": "input_value",
          "name": "x", 
          "check": "Number"
        },
        {
          "type": "input_value",
          "name": "y", 
          "check": "Number"
        },
        {
          "type": "input_value",
          "name": "radius", 
          "check": "Number"
        }
      ],
      "inputsInline": true,
      "previousStatement": null,
      "nextStatement": null,
          "colour": 30,
      "tooltip": "Draws a circle with radius r at point x/y.",
      "helpUrl": "https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D/arc"
    });
  }
};
