Blockly.JavaScript['draw_line'] = function(block) {
  var from_x = Blockly.JavaScript.valueToCode(block, 'from_x', Blockly.JavaScript.ORDER_ATOMIC) || 0;
  var from_y = Blockly.JavaScript.valueToCode(block, 'from_y', Blockly.JavaScript.ORDER_ATOMIC) || 0;
  var to_x = Blockly.JavaScript.valueToCode(block, 'to_x', Blockly.JavaScript.ORDER_ATOMIC) || 0;
  var to_y = Blockly.JavaScript.valueToCode(block, 'to_y', Blockly.JavaScript.ORDER_ATOMIC) || 0;
	var code = `
    var ctx = canvas.getContext('2d');

		ctx.beginPath();
		ctx.moveTo(${from_x}, ${from_y});
		ctx.lineTo(${to_x}, ${to_y});
		ctx.stroke();`;

  return code;
};

Blockly.JavaScript['draw_circle'] = function(block) {
  var x = Blockly.JavaScript.valueToCode(block, 'x', Blockly.JavaScript.ORDER_ATOMIC) || 0;
  var y = Blockly.JavaScript.valueToCode(block, 'y', Blockly.JavaScript.ORDER_ATOMIC) || 0;
  var radius = Blockly.JavaScript.valueToCode(block, 'radius', Blockly.JavaScript.ORDER_ATOMIC) || 0;
	var code = `
    var ctx = canvas.getContext('2d');

		ctx.beginPath();
		ctx.arc(${x}, ${y}, ${radius}, 0, 2 * Math.PI);
		ctx.stroke();`;

  return code;
};
