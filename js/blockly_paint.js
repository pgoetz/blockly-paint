Blockly.JavaScript.addReservedWords('code');

var workspace = Blockly.inject('editor',
	{	
		media: './media/',
		toolbox: document.getElementById('toolbox'), 
		trashcan: true
	}
);

Blockly.Xml.domToWorkspace(document.getElementById('startBlocks'), workspace);

document.querySelector('#execute').addEventListener('click', function() {
	var code = Blockly.JavaScript.workspaceToCode(workspace);

	console.log(code);

	var canvas = document.getElementById('output');
	
	if (canvas.getContext) {
		var context = canvas.getContext('2d');
		context.clearRect(0, 0, canvas.width, canvas.height);

		try {
			eval(code);
		} catch (e) {
			alert(e);
		}
	}
});
