= Drawing with Blockly

This simple project lets you draw on an HTML5 canvas using https://developers.google.com/blockly[Blockly]. 

== Usage

Just open the file `index.html` in a browser and start "programming" your image with the graphical blocks on the left side of the screen. 
Click "Ausführen" to create the resulting image on the canvas at the right side of the screen. 

== Sandbox on Gitlab Pages

If you want to try it out first please visit https://pgoetz.gitlab.io/blockly-paint/. 
